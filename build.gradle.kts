plugins {
    kotlin("jvm")
}

group = "vanadis.vanadis.discord"
version = "1.0.0"

dependencies {

    implementation(kotlin("stdlib-jdk8"))

    implementation(project(":vanadis:core"))

    // Use the Kotlin test library.
//    testImplementation(kotlin("kotlin-test"))

    // Use the Kotlin JUnit integration.
//    testImplementation(kotlin("kotlin-test-junit"))
}