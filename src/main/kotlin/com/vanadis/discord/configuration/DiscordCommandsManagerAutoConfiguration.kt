package com.vanadis.discord.configuration

import com.vanadis.core.configuration.manager.CommandsManagerConfigurer
import com.vanadis.core.configuration.manager.settings.ManagerConfigurer
import com.vanadis.core.command.provider.DefaultCommandProvider
import com.vanadis.discord.DiscordCommandsManager
import com.vanadis.discord.annotation.DiscordCommand
import com.vanadis.discord.command.SimpleDiscordCommand
import org.springframework.context.annotation.Configuration

@Configuration
open class DiscordCommandsManagerAutoConfiguration : CommandsManagerConfigurer() {

    override fun configure(managerConfigurer: ManagerConfigurer) {
        managerConfigurer
                .commandsHandling()
                .addCommandAnnotation(DiscordCommand::class)
                .setCommandProvider(DefaultCommandProvider::class)
                .setManager(DiscordCommandsManager::class, SimpleDiscordCommand::class)
    }
}