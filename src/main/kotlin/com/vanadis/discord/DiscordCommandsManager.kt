package com.vanadis.discord

import com.vanadis.core.command.annotation.CommandsManager
import com.vanadis.core.configuration.manager.events.AfterCommandsInitListener
import com.vanadis.core.configuration.manager.events.AllCommandListener
import com.vanadis.core.configuration.manager.events.AfterInitCommandsListener
import com.vanadis.core.command.provider.CommandProvider
import com.vanadis.discord.command.SimpleDiscordCommand
import org.springframework.beans.factory.annotation.Autowired

@CommandsManager
class DiscordCommandsManager : AllCommandListener<SimpleDiscordCommand>,
        AfterInitCommandsListener<SimpleDiscordCommand>,
        AfterCommandsInitListener {

    private lateinit var commands: Collection<CommandProvider<SimpleDiscordCommand>>

    override fun handleCommands(commands: Collection<CommandProvider<SimpleDiscordCommand>>) {
        this.commands = commands

        println("Bulk commands: $commands")
    }

    override fun handleInitializedCommand(command: CommandProvider<SimpleDiscordCommand>) {
        println("Initialized command: ${command.commandInstance}")
    }

    override fun afterCommandsInit() {
        println("All commands initialized!")
    }
}