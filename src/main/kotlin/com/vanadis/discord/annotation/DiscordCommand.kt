package com.vanadis.discord.annotation

import org.springframework.stereotype.Component

@Component
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class DiscordCommand(
        val command: String,
        val commandHelp: String = "<no help found>"
)